## Project Intro

This app is meant for the demo purpose.

## About the app
1. Technology Used: "React-Native".
2. Editor Used: VS Code

## Start running the app
1. Clone the repository
2. Install dependencies: "npm install"
3. In order to run it, first start the ADV simulator from android studio, then run command: 
    "react-native run-android"
    inside the project folder.
4. This will launch the app in the simulator.

## Clone a repository
