import {createStackNavigator, createAppContainer} from 'react-navigation';

import LoginPage  from './components/login_component';
import LandingPage from './components/landing_component';
import ViewProfilePage from './components/view_details_component';
import UpdateProfilePage from './components/update_details_components';

const landingStackRouteConfig = {
  Edit: {
    screen: ViewProfilePage
  },
  Update: {
    screen: UpdateProfilePage
  }
}

const landingStackNavigationOptions = {
  initialRouteName: "Edit",
  /* The header config from HomeScreen is now here */
  navigationOptions: {
    headerStyle: {
      backgroundColor: "#f4511e"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    },
  }
}

const LandingStack = createStackNavigator(
  landingStackRouteConfig,
  landingStackNavigationOptions
);

const rootStackRouteConfig = {
  Login: {
    screen: LoginPage
  },
  Home: {
    screen: LandingPage,
  },
  Landing: {
    screen: LandingStack
  }
}

const rootStackNavigationOptions = {
  headerMode: "none"
}

const RootStack = createStackNavigator(
  rootStackRouteConfig,
  rootStackNavigationOptions
);

const AppNavigator = createAppContainer(RootStack);

export default AppNavigator;