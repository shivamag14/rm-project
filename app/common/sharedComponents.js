import React from 'react';
import { View, StyleSheet, Text, TouchableHighlight } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export const Button = (props) => {
    const {handleEvent, button_text, show_image} = props;
    return (
    <TouchableHighlight onPress={handleEvent} style={styles.buttonTouchableStyle} underlayColor={'#f8f8f8'}>
        <View style={styles.buttonContainerStyle}>
            <Text style={styles.buttonTextStyle}>{button_text}</Text>
            {show_image ? <Icon style={styles.buttonIconStyle} name="arrow-right" size={25} color="#ffffff" /> : null}
        </View>
    </TouchableHighlight>
    );
}

export const InputTextField = (props) => {
    const {handleChangeEvent, value, label} = props;
    return (
        <View style={{paddingBottom: 10}}>
            <TextField
                label={label}
                value={value}
                style={styles.textField}
                tintColor= {'grey'}
                baseColor= {'#909090'}
                textColor = {'#303030'}
                labelFontSize = {16}
                labelPadding = {20}
                labelTextStyle= {{ fontWeight: 'bold' }}
                onChangeText={ handleChangeEvent }
            />
        </View>
    );
}

export const TextValueField = (props) => {
    const { value, label, handleUpdateEvent} = props;
    return (
        <View style={styles.TextValueFieldWrapper}>
            <View>
                <Text style={{ fontSize: 16 }}>{label}</Text>
                <Text style={{ color: '#000000', fontSize: 20, fontWeight: 'bold' }}>{value}</Text>
            </View>
            <View style={{flex: 1}}></View>
            <TouchableHighlight onPress={handleUpdateEvent} underlayColor={'#f8f8f8'}>
                <View >
                    <Text style={{ color: 'grey', fontSize: 16, fontWeight: '400' }}>Edit</Text>
                </View>
            </TouchableHighlight>
        </View>
    );
}

let styles = StyleSheet.create({
    buttonTouchableStyle:{
        backgroundColor: 'black',
        padding: 10,
        height: 60
    },
    buttonContainerStyle: {
        display: "flex", 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    buttonTextStyle: {
        color: '#fff', 
        fontSize: 20
    },
    buttonIconStyle: {
        paddingLeft: 20,
        fontSize: 22
    },
    textField: {
        fontWeight: 'bold',
        fontSize: 20
    },
    TextValueFieldWrapper: {
        paddingHorizontal: 20, 
        flexDirection: 'row', 
        alignItems: 'center', 
        borderStyle: 'solid', 
        backgroundColor: "#ffffff", 
        height: 90, 
        marginVertical: 10
    }
});