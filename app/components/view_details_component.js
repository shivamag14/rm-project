import React, {Component} from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Button, InputTextField, TextValueField } from '../common/sharedComponents';
import { setItem, getItem, removeItem } from '../utils/storage'; 

import Icon from 'react-native-vector-icons/dist/FontAwesome';

class ViewProfilePage extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: ''
        }
    }

    componentDidMount() {
      getItem('userData').then(data => {
        this.setState({
          firstName: data.firstName,
          lastName: data.lastName
        });
      });
    }

    static navigationOptions = ({ navigation, navigationOptions }) => {
        return {
          // headerTitle instead of title
          headerTitle: (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingRight: 50 }}><Text style={{ fontSize: 20, fontWeight: "bold" }}>Edit Personal Details</Text></View>),
          headerTitleStyle: {alignSelf: 'center'},
          headerLeft: (
            <Icon style={{paddingLeft: 20, fontSize: 22, fontWeight: '100'}} name="arrow-left" size={25} color="#000000" onPress={() => navigation.navigate('Home')}/>
          ),
          /* These values are used instead of the shared configuration! */
          headerStyle: {
            backgroundColor: "#ffffff",
          },
          headerTintColor: "#3c3c3c"
        };
    };

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
              <View>
                <TextValueField 
                    label={'First Name'}
                    value={this.state.firstName}
                    handleUpdateEvent= {()=> this.props.navigation.navigate('Update', {
                      field: 'firstName',
                      value: this.state.firstName
                    })}
                />

                <TextValueField 
                  label={'Last Name'}
                  value={this.state.lastName}
                  handleUpdateEvent= {()=> this.props.navigation.navigate('Update', {
                    field: 'lastName',
                    value: this.state.lastName
                  })}
                />
              </View>
              <View style={ styles.logoutWrapper }>
                <Text style={styles.logoutText} onPress={() => {
                    removeItem('userData');
                    navigation.navigate('Login')}}>Logout</Text>
              </View>
              <View style={{flex: 1}}></View>
              <View style={styles.footer}>
                <Text style={styles.footerText}>Demo App</Text>
                <Text style={styles.footerText}>v102</Text>
              </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingBottom: 10,
      justifyContent: 'flex-start',
      backgroundColor: '#f5f5f5',
    },
    logoutWrapper: { 
      flex: 1, 
      justifyContent: 'center', 
      alignItems: 'center' 
    },
    logoutText: { 
      color: '#d8232a', 
      fontSize: 20 
    },
    footer: { 
      paddingBottom: 20, 
      justifyContent: 'center', 
      alignItems: 'center' 
    },
    footerText: { 
      color: 'grey', 
      fontSize: 16, 
      fontWeight: '400' 
    }
  });

export default ViewProfilePage;