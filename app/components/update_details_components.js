import React, {Component} from 'react';
import { StyleSheet, View, Text, Keyboard } from 'react-native';
import { Button, InputTextField } from '../common/sharedComponents';
import { setItem, getItem, removeItem } from '../utils/storage'; 

import Icon from 'react-native-vector-icons/dist/FontAwesome';

class UpdateProfilePage extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: ''
        }
    }

    componentDidMount() {
      getItem('userData').then(data => {
        this.setState({
          firstName: data.firstName,
          lastName: data.lastName
        });
      });
    }

    static navigationOptions = ({ navigation, navigationOptions }) => {    
        return {
          // headerTitle instead of title
          headerTitle: (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingRight: 50 }}><Text style={{ fontSize: 20, fontWeight: "bold" }}>Update your Name</Text></View>),
          headerLeft: (
            <Icon style={{paddingLeft: 20, fontSize: 22, fontWeight: '100'}} name="arrow-left" size={25} color="#000000" onPress={() => navigation.navigate('Edit')}/>
          ),
          headerStyle: {
            backgroundColor: "#ffffff"
          },
          headerTitleStyle: { textAlign: 'center' },
          headerTintColor: "#3c3c3c"
        };
    };

    handleSave = () => {
      Keyboard.dismiss();
      
      if(this.props.navigation.state.params.field === 'firstName'){
        getItem('userData').then(data => {
          this.setState({
            firstName: this.state.firstName,
            lastName: data.lastName
          }, () => {
            setItem('userData', {
              firstName: this.state.firstName,
              lastName: this.state.lastName
            });
          });
        })
        this.props.navigation.navigate('Home');
      }else if(this.props.navigation.state.params.field === 'lastName'){
        getItem('userData').then(data => {
          this.setState({
            firstName: data.firstName,
            lastName: this.state.lastName,
          }, () => {
            setItem('userData', {
              firstName: this.state.firstName,
              lastName: this.state.lastName
            });
          });
        })
        this.props.navigation.navigate('Home');
      }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.fieldWrapper}>
                  { this.props.navigation.state.params.field === 'firstName' && <InputTextField 
                        label= 'First Name'
                        value= {this.state.firstName}
                        handleChangeEvent = {(value) => this.setState({ firstName: value })}
                    />
                  }
                  {this.props.navigation.state.params.field === 'lastName' && <InputTextField 
                        label= 'Last Name'
                        value= {this.state.lastName}
                        handleChangeEvent = {(value) => this.setState({ lastName: value })}
                    />
                  }  
                </View>
                <View style={{flex: 1}}></View>
                <Button
                    button_text={'Save'} show_image={false} handleEvent={this.handleSave}/>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 30,
      paddingBottom: 10,
      justifyContent: 'flex-start',
      backgroundColor: '#F5FCFF',
    },
    fieldWrapper: {
      paddingTop: 30, 
      paddingBottom: 20 
    }
  });

export default UpdateProfilePage;