import React, {Component} from 'react';
import { StyleSheet, View, Text, Keyboard, CheckBox, AsyncStorage} from 'react-native';
import { Button, InputTextField } from '../common/sharedComponents';

import { setItem, getItem, removeItem } from '../utils/storage'; 

class LoginPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            consent: false
        }
    }

    componentDidMount() {
        getItem('userData').then(data => {
            if(data !== null && data !== undefined){
                this.props.navigation.navigate('Home');
            }
          });
    }

    static navigationOptions = ({ navigation, navigationOptions }) => {
        const { params } = navigation.state;
    
        return {
          // headerTitle instead of title
          headerTitle: "Home",
          /* These values are used instead of the shared configuration! */
          headerStyle: {
            backgroundColor: "red"
          },
          headerTintColor: "#3c3c3c"
        };
    };

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    handleSubmit = () => {
        Keyboard.dismiss();
        let data = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            consent: this.state.consent
        }
        if(this.state.firstName.trim() !== "" && this.state.lastName.trim() !== "" && this.state.consent !== ""){
            setItem('userData', data);
            this.props.navigation.navigate('Home');
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcomeText}>
                    Hey! What should we call you?
                </Text>
                <View style={{paddingTop: 30, paddingBottom: 30}}>
                    <InputTextField 
                        label= 'First Name'
                        value= {this.state.firstName}
                        handleChangeEvent = {(value) => this.setState({ firstName: value })}
                    />
                    <InputTextField 
                        label= 'Last Name'
                        value= {this.state.lastName}
                        handleChangeEvent = {(value) => this.setState({ lastName: value })}
                    />
                </View>
                <View style={{ flex: 1 , flexDirection: 'row'}}>
                    <CheckBox style={{ color: '#000000'}} value={this.state.consent} onValueChange={value => this.setState({ consent : !this.state.consent })}/>
                    <Text style={{ color: '#909090', fontWeight: 'bold', fontSize: 15 }}> By proceeding I agree with <Text style={{ textDecorationLine: 'underline' }}>Terms & Conditions</Text>, <Text style={{ textDecorationLine: 'underline' }}>Terms of use</Text> and <Text style={{ textDecorationLine: 'underline' }}>Privacy Policy</Text></Text>
                </View>
                <View style={{flex: 1}}></View>
                <Button
                    button_text={'Continue'} show_image={true} handleEvent={this.handleSubmit}/>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 30,
      paddingBottom: 10,
      justifyContent: 'flex-start',
      backgroundColor: '#F5FCFF',
    },
    welcomeText: {
      fontSize: 22,
      fontFamily: 'Cochin',
      fontWeight: 'bold',
      textAlign: 'left',
      color: '#303030'
    }
  });

export default LoginPage;