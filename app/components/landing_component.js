import React, {Component} from 'react';
import { StyleSheet, View, Text, ImageBackground, Image, TouchableHighlight, FlatList } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {data as movieData} from '../data/app.data';

const toCamelCase = (str) => {
    return str.replace(/([-_][a-z])/ig, (data) => {
      return data.toUpperCase()
        .replace('-', '')
        .replace('_', '');
    });
  };

class LandingPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            consent: false,
            refreshing: false,
            teaserList: {}
        }
    }

    componentDidMount() {
        this.setState({
            teaserList: movieData.movieDetails.movieTeaserList
        });
    }
    
    _renderItem = ({item}) => {
        return (
            <View >
                <View style={styles.itemTitle}>
                    <Text style={styles.itemTitleText}>{item.title}</Text>
                </View>
                <Image source={ item.img } style={{ height: 150, width: '100%' }} loadingIndicatorSource={require('../assets/images/loader.gif')}/>
                <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10, alignItems: 'center' }}>
                    <View style={{ height: 60, justifyContent: 'space-around' }}>
                        <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 18, paddingVertical: 6}}>{item.heading}</Text>
                        <Text style={{ fontWeight: '200', fontSize: 14, paddingBottom: 5 }}>{item.gener}&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp; {item.views}&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;{item.time}</Text>
                    </View>
                    <View style={{flex: 1}}></View>
                    <View>
                        <Image source={require('../assets/images/play_btn.png')} style={{ width: 35, height: 35 }} />
                    </View>
                </View>
            </View>
        );
    };

    _keyExtractor = (item, index) => item.id;

    handleRefresh = () => {
        this.setState({
            teaserList: {},
            refreshing: true
        }, () => {
            this.setState({
                refreshing: false,
                teaserList: movieData.movieDetails.movieTeaserList
            })
        });
    }

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../assets/images/main_img.jpg')} loadingIndicatorSource={require('../assets/images/loader.gif')} style={{ flex: 1.1, alignItems: 'center'}}>
                    <Text style={styles.pullToRefreshText}>
                        Pull to refresh
                    </Text>
                    <View style={{flex: 1}}></View>
                    {/* for gradient layout */}
                    <LinearGradient colors={['#00000000', '#000000' ]} style={styles.linearGradient}>
                        <View style={{flex: 1}}></View>
                        <Text style={{ color: '#ffffff', fontSize: 35, fontWeight: 'bold', paddingBottom: 6 }}>
                            {toCamelCase(movieData.movieDetails.movieName)}
                        </Text>
                        <View style={{ flex: 1, flexDirection: 'row' , justifyContent: 'space-around'}}>
                            <Text style={{ color: '#ffffff', fontSize: 17 }}>{movieData.movieDetails.releaseYear} &nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;</Text>
                            <Text style={{ color: '#ffffff', fontSize: 17 }}>{movieData.movieDetails.totalViews} &nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;</Text>
                            <Text style={{ color: '#ffffff', fontSize: 17 }}>{movieData.movieDetails.movieDuration}</Text>
                        </View>
                        <TouchableHighlight onPress={()=> navigation.navigate('Edit')} style={{ height: 45, width: 170 , backgroundColor: '#ffffff', borderRadius: 5 }} underlayColor={'#f8f8f8'}>
                            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/images/play_btn.png')} style={{ width: 23, height: 23 }} />
                                <Text style={{ color: '#000000', fontWeight: 'bold', fontSize: 18, paddingHorizontal: 7 }}>{'Watch Trailer'}</Text>
                            </View>
                        </TouchableHighlight>
                    </LinearGradient>
                </ImageBackground>
                <FlatList
                    style={{flex: 1}}
                    data={this.state.teaserList}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF'
    },
    pullToRefreshText: {
      fontSize: 16,
      padding: 10,
      fontFamily: 'Cochin',
      fontWeight: 'bold',
      textAlign: 'left',
      color: '#ffffff'
    },
    linearGradient:{ 
        height: 180, 
        width: '100%', 
        justifyContent: 'space-evenly', 
        alignItems: 'center', 
        paddingBottom: 5,
        backgroundColor: 'transparent'
    },
    itemTitle: { 
        height: 60, 
        backgroundColor: 'black', 
        justifyContent: 'center' 
    },
    itemTitleText: { 
        color: '#ffffff', 
        fontSize: 16, 
        paddingLeft: 20, 
        fontWeight: '200'
    }
  });

export default LandingPage;