export const  data = {
    "movieDetails": {
        "movieMainImg": "../assets/images/main_img.jpg",
        "movieName": "Tangled",
        "releaseYear": 2010,
        "totalViews": "7+",
        "movieDuration": "1h 40m",
        "movieTeaserList": [
            {
                "id": "1",
                "title" : "TEASER 1",
                "img" : require('../assets/images/teaser1.jpg'),
                "heading": "Tangled - I See the Light (HD)",
                "gener": "Archiebuild",
                "views": "6m+ Views",
                "time": "7 years ago"
            },
            {
                "id": "2",
                "title" : "TEASER 2",
                "img" : require('../assets/images/teaser2.jpg'),
                "heading": "Tangled - Let Me Go (HD)",
                "gener": "Archie",
                "views": "2m+ Views",
                "time": "4 years ago"
            }
    
        ]
    }
}